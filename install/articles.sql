##ARTICLES TABLE 
CREATE TABLE articles (
  id int(11) NOT NULL,
  created date NOT NULL,
  updated date NOT NULL,
  created_by int(11) NOT NULL,
  summary text DEFAULT NULL,
  title varchar(255) NOT NULL,
  subheading varchar(255) DEFAULT NULL,
  url varchar(255) NOT NULL,
  category_id int(11),
  body text DEFAULT NULL,
  keywords varchar(80) DEFAULT NULL,
  author_id int(11) NOT NULL,
  created_on timestamp,
  updated_on timestamp,
  description varchar(255) DEFAULT NULL,
  status enum('LIVE', 'DRAFT'),
  weight int(11),
  featured_image varchar(255) DEFAULT NULL,
  featured_image_raw varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);
